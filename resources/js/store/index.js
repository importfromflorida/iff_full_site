export default {

    state: {
        isAuth: false,
        categories: [],
        products: [],
        searchQuery: null,
        searchCategory: null,
        showToast: true
    },

    getters: {
        // nonOtherCategories(state) { // Return all categories expect the Other category
        //     return state.categories.filter(category => category.name.toLowerCase() != 'other')
        // },

        // otherCategory(state) { // Return other category
        //     return state.categories.filter(category => category.name.toLowerCase() === 'other')
        // },

        searchResults(state) { // Return products based on search category and search query
            // Is search category null?
            if (state.searchCategory) { // No
                //Is search query null?
                if (state.searchQuery) { // No
                    const results = state.products.filter(product => {
                        return product.categories.some(category => category.name.toLowerCase() === state.searchCategory.toLowerCase()) &&
                            product.name.toLowerCase().includes(state.searchQuery.toLowerCase())
                    })

                    return results.length > 0 ? results : null
                } else { // Yes
                    const results = state.products.filter(product => {
                        return product.categories.some(category => category.name.toLowerCase() === state.searchCategory.toLowerCase());
                    })

                    return results.length > 0 ? results : null
                }
            } else { // Yes
                //Is search query null?
                if (state.searchQuery) { // No
                    const results = state.products.filter(product => {
                        return product.name.toLowerCase().includes(state.searchQuery.toLowerCase())
                    })

                    return results.length > 0 ? results : null
                } else { // Yes
                    return state.products.length > 0 ? state.products : null;
                }
            }
        }
    },

    mutations: {
        setIsAuth(state, value) {
            state.isAuth = value
        },

        setCategories(state, data) {
            state.categories = data
        },
        setProducts(state, data) {
            state.products = data

        },
        setShowToast(state, value) {
            state.showToast = false
        },
        setSearchQuery(state, value) {
            state.searchQuery = value
        },
        setSearchCategory(state, value) {
            state.searchCategory = value
        }

    },
    actions: {
        setIsAuth(context, value) {
            context.commit("setIsAuth", value)
        },
        setShowToast(context, value) {
            context.commit("setShowToast", value)
        },
        async getCategories(context) {
            const {
                data
            } = await axios.get('api/categories')

            context.commit('setCategories', data)

        },

        async getProducts(context) {
            const {
                data
            } = await axios.get('api/products')

            context.commit('setProducts', data)
        },

        setSearchQuery(context, value) {
            context.commit('setSearchQuery', value)
        },
        setSearchCategory(context, value) {
            context.commit('setSearchCategory', value)
        },
    }
}