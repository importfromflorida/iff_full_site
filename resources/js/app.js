/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
const VueScrollTo = require('vue-scrollto');
require('./bootstrap');
window.Vue = require('vue');
import VueRouter from 'vue-router'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueScrollTo)

import storeData from "./store/index"

const store = new Vuex.Store(
    storeData
)


/** FRONT END ROUTES BELOW */

const router = new VueRouter({
    mode: 'history',
    scrollBehavior(to, from) {
        return {
            x: 0,
            y: 0
        }
    }
});
// Vue.use(VueScrollTo);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('detail-component', require('./components/admin/ProductDetailComponent.vue').default);
Vue.component('product-list', require('./components/admin/ProductList.vue').default);
Vue.component('order-list', require('./components/admin/OrderList.vue').default);

// FRONT END SECTION
Vue.component('nav-component', require('./components/headerNav.vue').default);
Vue.component('foot-component', require('./components/foot.vue').default);
Vue.component('slider-component', require('./components/slider.vue').default);
Vue.component('search-component', require('./components/search.vue').default);
Vue.component('categories-component', require('./components/categories.vue').default);
Vue.component('about-component', require('./components/about.vue').default);
Vue.component('store-component', require('./views/store.vue').default);
Vue.component('product-component', require('./views/theproductPage.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const adminApp = new Vue({
    el: '#admin-app',
});

const app = new Vue({
    el: '#app',
    router

});