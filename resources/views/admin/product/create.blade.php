@extends('layouts.app')

@section('title', 'Create New Product')

@section('content')
<div class="container product-form-container">
    <div class="card">

        <div class="card-content">
            <form class="col s12" method="POST" action="{{ route('admin.product.store')}}"
                enctype="multipart/form-data">
                @csrf

                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('name') }}" placeholder="Product Name" id="product_name" type="text"
                            name="name" class="validate" autocomplete="off" required>
                        <label for="product_name">Name</label>
                        @error('name')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('price') }}" placeHolder="Product Price" id="product_price" name="price"
                            type="number" step="0.01" class="validate" required>
                        <label for="product_price">Price</label>

                        @error('price')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('quantity') }}" placeholder="Product Quantity" id="product_quantity"
                            type="number" name="quantity" class="validate" required>
                        <label for="product_quantity">Quantity</label>

                        @error('quantity')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <select id="select-box" name="categories[]" multiple required>
                            <option value="" disabled selected>Choose one or more categories</option>
                            @foreach ($catergories as $category)
                            <option value="{{$category->id}}">{{$category->name}}
                            </option>

                            @endforeach


                        </select>
                        <label>Categories</label>

                        @error('categories')
                        <span class="form-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <label>
                            <input type="checkbox" id="vehicle-checkbox" name="isVehicle" />
                            <span>Vehicle</span>
                        </label>
                    </div>
                </div>


                <div class="row">
                    <div class="input-field col s12">
                        <select name="condition">
                            <option value="" disabled selected>Choose condition</option>
                            <option value="New">New</option>
                            <option value="Used">Used</option>
                            <option value="Refurbished">Refurbished</option>

                        </select>

                        <label>Condition</label>

                        @error('condition')
                        <span class="form-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="description" class="materialize-textarea" name="description"
                            placeHolder="Category Description" data-length="120">{{ old('description') }}</textarea>
                        <label for="description">Description</label>


                        @error('description')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row" id="form-vehicle-info-section" hidden>
                    <div class="input-field col s12">
                        <input value="{{ old('mileage') }}" placeholder="Vehicle Mileage" id="product_mileage"
                            type="number" name="mileage" class="validate">
                        <label for="product_mileage">Mileage</label>

                        @error('mileage')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="input-field col s12">
                        <input value="{{ old('color') }}" placeholder="Vehicle Color" id="product_color" type="text"
                            name="color" class="validate">
                        <label for="product_color">Color</label>

                        @error('color')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="input-field col s12">

                        <input type="text" class="datepicker" placeholder="Manufactured Date" name="manufactured"
                            ud="product_manufacturd">
                        <label for="product_manufactured">Date</label>

                        @error('manufactured')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>



                </div>

                <div class="row">
                    <div class="file-field input-field col s12">
                        <div class="btn waves-effect waves-light indigo ">
                            <span>Browse</span>
                            <input type="file" name="images[]" accept="image/x-png,image/jpeg" autofocus multiple
                                required>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload one or more images">
                        </div>

                        @error('images')
                        <span class="form-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col s12" style="display:flex; justify-content:flex-end;"><button
                            class="btn waves-effect waves-light indigo" type="submit" name="action">Add Product
                        </button></div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

<script type="text/javascript" src="{{asset('js/productform.js')}}"></script>

<style>
    .product-form-container {
        margin-top: 6rem !important;
    }

    .form-error {
        color: red;
        /* margin-left: 1rem; */
    }

</style>
