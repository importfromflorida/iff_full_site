@extends('layouts.app')

@section('title', 'Product Edit')

@section('content')
<div class="container product-form-container">
    <div class="card">

        <div class="card-content">
            <form class="col s12" method="POST" action="{{ route('admin.product.update', $product->id)}}">
                @csrf
                @method('PATCH')

                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('name', $product->name) }}" placeholder="Product Name" id="product_name"
                            type="text" name="name" class="validate" autocomplete="off" required>
                        <label for="product_name">Name</label>


                        @error('name')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('price', $product->price) }}" placeHolder="Product Price"
                            id="product_price" name="price" type="number" step="0.01" class="validate" required>
                        <label for="product_price">Price</label>
                        @error('price')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('quantity', $product->quantity) }}" placeholder="Product Quantity"
                            id="product_quantity" type="number" name="quantity" class="validate" required>
                        <label for="product_quantit">Quantity</label>

                        @error('quantity')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                </div>

                <div class="row">
                    <div class="col s12">
                        <label>
                            <input type="checkbox" id="vehicle-checkbox" name="isVehicle"
                                {{ $product->vehicle ? 'checked' : '' }} />
                            <span>Vehicle</span>
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <select name="condition" class="condition-select">
                            <option value="" disabled selected>Choose condition</option>
                            <option value="New" {{$product->condition === "New" ? 'selected' : ''}}>New</option>
                            <option value="Used" {{$product->condition === "Used" ? 'selected' : ''}}>Used</option>
                            <option value="Refurbished" {{$product->condition === "Refurbished" ? 'selected' : ''}}>
                                Refurbished
                            </option>
                        </select>

                        <label>Condition</label>

                        @error('condition')
                        <span class="form-error" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="description" class="materialize-textarea" name="description"
                            placeHolder="Description"
                            data-length="120">{{ old('description', $product->description) }}</textarea>
                        <label for="description">Description</label>


                        @error('description')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>

                @if ($product->vehicle)
                <div class="row" id="form-vehicle-info-section">
                    <div class="input-field col s12">
                        <input value="{{ old('mileage', $product->mileage) }}" placeholder="Vehicle Mileage"
                            id="product_mileage" type="number" name="mileage" class="validate">
                        <label for="product_mileage">Mileage</label>

                        @error('mileage')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="input-field col s12">
                        <input value="{{ old('color', $product->color) }}" placeholder="Vehicle Color"
                            id="product_color" type="text" name="color" class="validate">
                        <label for="product_color">Color</label>

                        @error('color')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="input-field col s12">

                        <input type="text" class="datepicker" placeholder="Manufactured Date" name="manufactured"
                            ud="product_manufacturd" value={{$product->manufactured}}>
                        <label for="product_manufactured">Date</label>

                        @error('manufactured')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                @endif
                <div class="row" style="display:flex; justify-content:flex-end;">
                    <button class="btn waves-effect waves-light indigo" type="submit" name="action">Save Product
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

<style>
    .product-form-container {
        margin-top: 6rem !important;
    }

    .form-error {
        color: red;
        /* margin-left: 1rem; */
    }

</style>
