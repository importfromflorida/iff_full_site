@extends('layouts.app')

@section('title', 'Products')


@section('content')
<div class="container main-list-container">



    <product-list :products-data="{{json_encode($products)}}"></product-list>


    {{-- <div class="row">
        <div class="col s12 center-align">
            <ul class="pagination">
                <li class="waves-effect {{$products->onFirstPage() ? 'disabled disable-clicks' : ''}}">
    <a href="{{$products->previousPageUrl()}}"><i class="material-icons">chevron_left</i></a>
    </li>
    @for ($i = 1; $i < $products->lastpage()+1; $i++)
        <li class="{{$products->currentPage() === $i ? 'blue darken-1' : ''}}"><a
                class="{{$products->currentPage() === $i ? 'white-text' : ''}}" href="{{$products->url($i)}}">{{$i}}</a>
        </li>
        @endfor

        <li
            class="waves-effect {{$products->currentPage() === $products->lastPage() ? 'disabled disable-clicks' : ''}}">
            <a href="{{$products->nextPageUrl()}}"><i class="material-icons">chevron_right</i></a>
        </li>
        </ul>
</div>
</div> --}}

</div>

@endsection



<style>
    .main-list-container {
        margin-top: 6rem !important;
    }

    .card-head {
        display: flex;
        justify-content: space-between
    }

    .disable-clicks {
        pointer-events: none;
    }

</style>
