@extends('layouts.app')

@section('title', 'Product Details')

@section('content')
<div class="row  main-details-container">
    <div id="model-delete" class="modal">
        <div class="modal-content center-align">
            <h4>Delete Product</h4>
            <p>Are you sure you want to delete this product?</p>
            <p>Doing so will delete all orders that have been placed for this product.</p>
            <p>Setting the product quantity to zero , will prevent the product from showing up in the store.</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
            <a class="modal-close waves-effect waves-red btn-flat" onclick="event.preventDefault();
               document.getElementById('frm-delete-{{$product->id}}').submit();">Delete</a>
            <form id="frm-delete-{{$product->id}}" action="{{ route('admin.product.destroy', $product->id) }}"
                method="POST" style="display: none;">
                {{ csrf_field() }}
                @method('DELETE')
            </form>
        </div>
    </div>
    <div class="col offset-s1 s10">

        <div class="row">
            <div class="col s12">
                <a class="waves-effect waves-light btn-small  orange darken-1"
                    href="{{ route('admin.product.edit', $product->id)}}">Edit</a>

                <a class="waves-effect waves-light btn-small  red darken-4 modal-trigger" href="#model-delete"><i
                        class="material-icons">delete</i></a>


            </div>
        </div>


        <div class="row">

            <detail-component title="Name" value="{{$product->name}}" color="blue-grey darken-1"></detail-component>
            <detail-component title="Price" value="{{$product->price}}" color="card green lighten-1"></detail-component>
            <detail-component title="Order(s)" :value="{{$product->orders}}" color="card pink lighten-1">
            </detail-component>
            <detail-component title="Quantity" value="{{$product->quantity}}" color="card blue lighten-1">
            </detail-component>
            <detail-component title="Condition" value="{{$product->condition}}" color="deep-purple lighten-1">
            </detail-component>

            @if ($product->vehicle)
            <detail-component title="Mileage" value="{{$product->mileage}}" color="indigo lighten-1">
            </detail-component>
            <detail-component title="Color" value="{{$product->color}}" color="teal lighten-1">
            </detail-component>
            <detail-component title="Manufactured Year" value="{{$product->formatDate($product->manufactured)}}"
                color="brown lighten-1">
            </detail-component>
            @endif
        </div>

        <div class="row">
            <div class="col s12">
                <ul class="collapsible">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">short_text</i>Description</div>
                        <div class="collapsible-body">
                            <p>{{$product->description ? $product->description : 'N/A' }}</p>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <ul class="collapsible">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">view_module</i>Categories</div>
                        <div class="collapsible-body">

                            <div class="row">
                                <div class="col s12">
                                    <form method="POST" action="{{route('admin.category.attach')}}">
                                        @csrf
                                        <input name="product_id" type="hidden" value="{{$product->id}}">
                                        <div class="row valign-wrapper">
                                            <div class="col s10">
                                                <select name="categories[]" multiple required>
                                                    <option value="" disabled selected>Choose one or more categories
                                                    </option>
                                                    @foreach ($nonCategories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach

                                                </select>
                                                @error('image')
                                                <span class="form-error" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror

                                            </div>
                                            <div class="col s2 center-align">
                                                <button class="btn-floating waves-effect waves-light indigo tooltipped"
                                                    type="submit" data-position="bottom" data-tooltip="Add category">
                                                    <i class="material-icons">add</i>
                                                </button>

                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <ul class="collection">
                                @foreach ($product->categories as $category)
                                <li class="collection-item">
                                    <div>{{ $category->name }}<a href="{{route('admin.category.show', $category->id)}}"
                                            class="secondary-content tooltipped" data-position="bottom"
                                            data-tooltip="Remove category"
                                            onclick="event.preventDefault(); document.getElementById('category-detach-{{$category->id}}').submit();"><i
                                                class="material-icons red-text text-darken-4">delete</i></a></div>

                                    <form id="category-detach-{{$category->id}}"
                                        action="{{ route('admin.category.detach', [$product->id , $category->id]) }}"
                                        method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                    </form>
                                </li>
                                @endforeach

                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        <div class="row">
            @if ($message = Session::get('success'))

            <div class="col s12 session-success-message" style="padding-bottom: 0 !important;">
                <ul class="collection " style="margin: 0 !important;">
                    <li class="collection-item green lighten-3 green-text text-darken-4" style="font-weight: bold">
                        <div>{{$message}}<a href="#!" class="secondary-content"
                                onclick="event.preventDefault(); document.querySelector('.session-success-message').style = 'display:none'"><i
                                    class="material-icons grey-text text-darken-1">close</i></a></div>
                    </li>
                </ul>
            </div>
            @endif
            <div class="col s12">
                <ul class="collapsible">
                    <li class="{{$errors->has('image') || Session::has('success') ? 'active' : ''}}">
                        <div class="collapsible-header"><i class="material-icons">image</i>Preview Images</div>
                        <div class="collapsible-body">

                            <div class="row">


                                @if ($product->limitChecker($product->images->count()))


                                <div class="col s12">
                                    <form method="POST" action="{{route('image.store')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input name="product_id" type="hidden" value="{{$product->id}}">
                                        <div class="row valign-wrapper">
                                            <div class="col s10">


                                                <div class="file-field input-field">
                                                    <div class="btn indigo">
                                                        <span>Browse</span>
                                                        <input type="file" name="images[]"
                                                            accept="image/x-png,image/jpeg" autofocus multiple required>
                                                    </div>
                                                    <div class="file-path-wrapper">
                                                        <input class="file-path validate" type="text"
                                                            placeholder="Upload an image">
                                                    </div>
                                                </div>

                                                @error('images')
                                                <span class="form-error" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror

                                            </div>
                                            <div class="col s2 center-align">
                                                <button class="btn-floating waves-effect waves-light indigo tooltipped"
                                                    type="submit" data-position="bottom" data-tooltip="Add new image">
                                                    <i class="material-icons">add</i>
                                                </button>

                                            </div>
                                        </div>
                                    </form>
                                </div>


                                @endif

                                @foreach ($product->images as $image)

                                <div class="col s12 m3">
                                    <div class="card">
                                        <div class="card-image">
                                            <img class="materialboxed" width="100%" src="{{$image->uri}}">
                                            {{-- <span class="card-title">{{$index}}</span> --}}
                                            <a class="btn-floating halfway-fab waves-effect waves-light red tooltipped"
                                                data-position="bottom" data-tooltip="Delete image"
                                                onclick="event.preventDefault(); document.getElementById('img-delete-{{$image->id}}').submit();"><i
                                                    class="material-icons">delete</i></a>
                                        </div>

                                        <form id="img-delete-{{$image->id}}"
                                            action="{{ route('image.destroy', $image->id) }}" method="POST"
                                            style="display: none;">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content card-head  pink lighten-1">
                        <span class="card-title" style="color:#FFF;">Order List</span>
                        {{-- <a class="waves-effect waves-light btn  teal darken-2" href="{{ route('admin.product.create')}}">ADD
                        NEW
                        PRODUCT</a> --}}
                    </div>
                    <div class="card-action">
                        <table class="highlight centered responsive-table">


                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone#</th>
                                    <th>Status</th>
                                    <th>Date Ordered</th>
                                    <th>Date Updated</th>
                                    <th>Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @foreach ($product->orders as $order)
                                <tr>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->email}}</td>
                                    <td>{{$order->phone }}</td>
                                    <td>{{ $order->status}}</td>
                                    <td>{{ $order->formatDate($order->created_at)}}</td>
                                    <td>{{ $order->formatDate($order->updated_at)}}</td>
                                    <td>

                                        <a class="waves-effect waves-light btn-small  indigo"
                                            href="{{ route('admin.order.show', $order->id)}}">View</a>

                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection


<style>
    .main-details-container {
        margin-top: 6rem !important;
    }

    .main-details-main-action-btns-container {
        display: flex;
        justify-content: flex-end;
        /* align-items: baseline; */
        /* margin: 1rem 0; */
        /* height: 100%; */

    }

    .main-details-main-action-btns-container a {
        margin-left: 5px;
    }

    .form-error {
        color: red;
    }

</style>
