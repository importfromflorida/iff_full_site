@extends('layouts.app')

@section('title', 'Create New Category')

@section('content')
<div class="container order-form-container">
    <div class="card">

        <div class="card-content">
            <form class="col s12" method="POST" action="{{ route('admin.category.store')}}">
                @csrf

                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('name') }}" placeholder="Category Name" id="category_name" type="text"
                            name="name" class="validate" autocomplete="off" required>
                        <label for="category_name">Name</label>

                        @error('name')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="description" class="materialize-textarea" name="description"
                            placeHolder="Category Description" data-length="120">{{ old('description') }}</textarea>
                        <label for="description">Description</label>

                        @error('description')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>

                </div>


                <div class="row" style="display:flex; justify-content:flex-end;">
                    <button class="btn waves-effect waves-light indigo" type="submit" name="action">ADD CATEGORY
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection



<style>
    .order-form-container {
        margin-top: 6rem !important;
    }

    .form-error {
        color: red;

    }

</style>
