@extends('layouts.app')

@section('title', 'Category Details')

@section('content')
<div class="row  main-details-container">
    <div class="col offset-s1 s10">

        <div class="row">
            <div class="col s12">
                <a class="waves-effect waves-light btn-small  orange darken-1"
                    href="{{ route('admin.category.edit', $category->id)}}">Edit</a>
                <a class="waves-effect waves-light btn-small  red darken-4"
                    onclick="event.preventDefault(); document.getElementById('frm-delete-{{$category->id}}').submit();"><i
                        class="material-icons">delete</i></a>

                <form id="frm-delete-{{$category->id}}" action="{{ route('admin.category.destroy', $category->id) }}"
                    method="POST" style="display: none;">
                    {{ csrf_field() }}
                    @method('DELETE')
                </form>
            </div>
        </div>


        <div class="row">

            <detail-component title="Name" value="{{$category->name}}" color="blue-grey darken-1"></detail-component>
            <detail-component title="Product(s)" :value="{{$category->products}}" color="card pink lighten-1">
            </detail-component>


            <div class="row">
                <div class="col s12">
                    <ul class="collapsible">
                        <li>
                            <div class="collapsible-header"><i class="material-icons">short_text</i>Description</div>
                            <div class="collapsible-body">
                                <p>{{$category->description ? $category->description : 'N/A' }}</p>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>


            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content card-head  pink lighten-1">
                            <span class="card-title" style="color:#FFF;">Product List</span>
                            {{-- <a class="waves-effect waves-light btn  teal darken-2" href="{{ route('admin.product.create')}}">ADD
                            NEW
                            PRODUCT</a> --}}
                        </div>
                        <div class="card-action">
                            <table class="highlight centered responsive-table">


                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Order(s)</th>
                                        <th>Quantity</th>

                                        <th>Action</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @foreach ($category->products as $product)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->price}}</td>
                                        <td>{{$product->orders->count() }}</td>
                                        <td>{{ $product->quantity}}</td>

                                        <td>

                                            <a class="waves-effect waves-light btn-small  blue darken-1"
                                                href="{{ route('admin.product.show', $product->id)}}">View</a>
                                            {{-- <a class="waves-effect waves-light btn-small  orange darken-1"
                                            href="{{ route('admin.order.edit', $order->id)}}">Edit</a>
                                            <a class="waves-effect waves-light btn-small  red darken-4"
                                                onclick="event.preventDefault(); document.getElementById('frm-delete-{{$order->id}}').submit();"><i
                                                    class="material-icons">delete</i></a>

                                            <form id="frm-delete-{{$order->id}}"
                                                action="{{ route('admin.order.destroy', $order->id) }}" method="POST"
                                                style="display: none;">
                                                {{ csrf_field() }}
                                                @method('DELETE')
                                            </form> --}}

                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection


    <style>
        .main-details-container {
            margin-top: 6rem !important;
        }

        .main-details-main-action-btns-container {
            display: flex;
            justify-content: flex-end;
            /* align-items: baseline; */
            /* margin: 1rem 0; */
            /* height: 100%; */

        }

        .main-details-main-action-btns-container a {
            margin-left: 5px;
        }

        .form-error {
            color: red;
        }

    </style>
