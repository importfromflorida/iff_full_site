@extends('layouts.app')

@section('title', 'Category Edit')

@section('content')
<div class="container product-form-container">
    <div class="card">

        <div class="card-action">
            <form class="col s12" method="POST" action="{{ route('admin.category.update', $category->id)}}">
                @csrf
                @method('PATCH')

                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('name', $category->name) }}" placeholder="Category Name" id="category_name"
                            type="text" name="name" class="validate" autocomplete="off">
                        <label for="category_name">Name</label>
                    </div>

                    @error('name')
                    <span class="form-error">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="description" class="materialize-textarea" name="description"
                            placeHolder="Category Description"
                            data-length="120">{{ old('description', $category->description) }}</textarea>
                        <label for="description">Description</label>
                    </div>


                    @error('description')
                    <span class="form-error">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="row" style="display:flex; justify-content:flex-end;">
                    <button class="btn waves-effect waves-light indigo" type="submit" name="action">Save Category
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

<style>
    .product-form-container {
        margin-top: 6rem !important;
    }

    .form-error {
        color: red;
        margin-left: 1rem;
    }

</style>
