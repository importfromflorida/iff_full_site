@extends('layouts.app')

@section('title', 'Categories')


@section('content')
<div class="container main-list-container">
    <div class="card">

        <div class="card-content">
            <table class="highlight centered responsive-table">


                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Product(s)</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>


                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{$category->products->count() }}</td>
                        <td>{{ $category->formatDate($category->created_at)}}</td>
                        <td>{{ $category->formatDate($category->updated_at)}}</td>
                        <td>

                            <a class="waves-effect waves-light btn-small  indigo"
                                href="{{ route('admin.category.show', $category->id)}}">View</a>


                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

    {{-- <div class="row">
        <div class="col s12 center-align">
            <ul class="pagination">
                <li class="waves-effect {{$categories->onFirstPage() ? 'disabled disable-clicks' : ''}}">
    <a href="{{$categories->previousPageUrl()}}"><i class="material-icons">chevron_left</i></a>
    </li>
    @for ($i = 1; $i < $categories->lastpage()+1; $i++)
        <li class="{{$categories->currentPage() === $i ? 'blue darken-1' : ''}}"><a
                class="{{$categories->currentPage() === $i ? 'white-text' : ''}}"
                href="{{$categories->url($i)}}">{{$i}}</a></li>
        @endfor

        <li
            class="waves-effect {{$categories->currentPage() === $categories->lastPage() ? 'disabled disable-clicks' : ''}}">
            <a href="{{$categories->nextPageUrl()}}"><i class="material-icons">chevron_right</i></a>
        </li>
        </ul>
</div>
</div> --}}

</div>
@endsection



<style>
    .main-list-container {
        margin-top: 6rem !important;
    }

    .card-head {
        display: flex;
        justify-content: space-between
    }

    .disable-clicks {
        pointer-events: none;
    }

</style>
