@extends('layouts.app')

@section('title', 'Place Order')

@section('content')
<div class="container order-form-container">
    <div class="card">
        <div class="card-content blue darken-1">
            <span class="card-title" style="color:#FFF;">Place Order</span>
        </div>
        <div class="card-action">
            <form class="col s12" method="POST" action="{{ route('admin.order.store')}}">
                @csrf
                <input name="product_id" type="hidden" value="{{$id}}">
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('name') }}" placeholder="Full Name" id="full_name" type="text" name="name"
                            class="validate" autocomplete="off" required>
                        <label for="full_name">Full Name</label>
                    </div>

                    @error('name')
                    <span class="form-error">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('email') }}" placeHolder="Email" id="email" name="email" type="email"
                            class="validate" required>
                        <label for="email">Email</label>
                    </div>

                    @error('email')
                    <span class="form-error">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('phone') }}" placeholder="Phone Number" id="phone" type="number"
                            name="phone" class="validate" required autocomplete="off">
                        <label for="phone">Phone#</label>
                    </div>

                    @error('phone')
                    <span class="form-error">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>


                <div class="row" style="display:flex; justify-content:flex-end;">
                    <button class="btn waves-effect waves-light blue darken-1" type="submit" name="action">Place
                        Order
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection



<style>
    .order-form-container {
        margin-top: 6rem !important;
    }

    .form-error {
        color: red;
        margin-left: 1rem;
    }

</style>
