@extends('layouts.app')

@section('title', 'Order Details')

@section('content')
<div class="row main-details-container">
    <div class="col s10 offset-s1">

        <div class="row">
            <div class="col s12 ">
                <a class="waves-effect waves-light btn-small  orange darken-1"
                    href="{{ route('admin.order.edit', $order->id)}}">Edit</a>
                <a class="waves-effect waves-light btn-small  red darken-4"
                    onclick="event.preventDefault(); document.getElementById('frm-delete-{{$order->id}}').submit();"><i
                        class="material-icons">delete</i></a>

                <form id="frm-delete-{{$order->id}}" action="{{ route('admin.order.destroy', $order->id) }}"
                    method="POST" style="display: none;">
                    {{ csrf_field() }}
                    @method('DELETE')
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <ul class="collection with-header">
                    <li class="collection-header indigo white-text">
                        <h4>Order#: {{ $order->id }}</h4>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Full Name</div>
                        <div class="info-item-content">{{$order->name}}</div>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Email</div>
                        <div class="info-item-content">{{$order->email}}</div>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Phone</div>
                        <div class="info-item-content">{{chunk_split($order->phone,3,' ')}}</div>
                    </li>

                    <li class="collection-item order-info-item info-item-product">
                        <div class="info-item-title">Product</div>
                        <div class="info-item-content">{{$order->product->name}}
                            <a class="waves-effect waves-light btn-small indigo"
                                href="{{ route('admin.product.show', $order->product->id)}}"
                                style="margin-left:5px;">View</a>
                        </div>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Status</div>
                        <div class="info-item-content">{{$order->status}}</div>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Questions</div>
                        <div class="info-item-content">{{$order->questions ? $order->questions : 'N/A' }}</div>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Date Ordered</div>
                        <div class="info-item-content">{{$order->formatShowDate($order->created_at) }}</div>
                    </li>
                    <li class="collection-item order-info-item">
                        <div class="info-item-title">Date Updated</div>
                        <div class="info-item-content"> {{$order->formatShowDate($order->updated_at)}}</div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    @endsection


    <style>
        .main-details-container {
            margin-top: 6rem !important;
        }

        .main-details-main-action-btns-container {
            display: flex;
            justify-content: flex-end;
            /* align-items: baseline; */
            /* margin: 1rem 0; */
            /* height: 100%; */

        }

        .main-details-main-action-btns-container a {
            margin-left: 5px;
        }

        .order-info-item {
            display: flex;
            padding: 0 !important;
        }

        .info-item-title {
            width: 120px;
            border-right: 1px solid #e0e0e0;
            /* padding: 10px 0; */
            font-weight: bold;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .info-item-content {
            width: calc(100% - 120px);
            padding: 10px 1rem 10px 1rem;
            display: flex;
            align-items: center;
            flex-wrap: wrap;
        }

    </style>
