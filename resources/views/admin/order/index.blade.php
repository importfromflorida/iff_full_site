@extends('layouts.app')

@section('title', 'Orders')


@section('content')
<div class="container main-list-container">


    <order-list :orders-data="{{json_encode($orders)}}"></order-list>

    {{-- <div class="row">
        <div class="col s12 center-align">
            <ul class="pagination">
                <li class="waves-effect {{$orders->onFirstPage() ? 'disabled disable-clicks' : ''}}">
    <a href="{{$orders->previousPageUrl()}}"><i class="material-icons">chevron_left</i></a>
    </li>
    @for ($i = 1; $i < $orders->lastpage()+1; $i++)
        <li class="{{$orders->currentPage() === $i ? 'blue darken-1' : ''}}"><a
                class="{{$orders->currentPage() === $i ? 'white-text' : ''}}" href="{{$orders->url($i)}}">{{$i}}</a>
        </li>
        @endfor

        <li class="waves-effect {{$orders->currentPage() === $orders->lastPage() ? 'disabled disable-clicks' : ''}}">
            <a href="{{$orders->nextPageUrl()}}"><i class="material-icons">chevron_right</i></a>
        </li>
        </ul>
</div>
</div> --}}
</div>
@endsection


<style>
    .main-list-container {
        margin-top: 6rem !important;
    }

    .card-head {
        display: flex;
        justify-content: space-between
    }

</style>
