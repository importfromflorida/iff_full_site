@extends('layouts.app')

@section('title', 'Order Edit')

@section('content')
<div class="container order-form-container">
    <div class="card">

        <div class="card-content">
            <form class="col s12" method="POST" action="{{ route('admin.order.update', $order->id)}}">
                @csrf
                @method('PATCH')

                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{$order->id }}" placeholder="Order Number" id="order_id" type="text" name="id"
                            class="validate" autocomplete="off" disabled>
                        <label for="order_id">Order#</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ $order->name }}" placeholder="Full Name" id="full_name" type="text" name="name"
                            class="validate" autocomplete="off" disabled>
                        <label for="full_name">Full Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ $order->product->name }}" placeholder="Full Name" id="product_name" type="text"
                            name="product_name" class="validate" autocomplete="off" disabled>
                        <label for="product_name">Product</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input value="{{ old('quantity', $order->quantity) }}" placeholder="Order Quantity"
                            id="order_quantity" type="number" name="quantity" class="validate" required>
                        <label for="order_quantit">Quantity</label>

                        @error('quantity')
                        <span class="form-error">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select name="status">
                            <option value="Pending" {{$order->status === 'Pending' ? 'selected' :''}}>Pending
                            </option>
                            <option value="Completed" {{$order->status === 'Completed' ? 'selected' :''}}>Completed
                            </option>
                        </select>
                        <label>Order Status</label>
                    </div>
                </div>

                <div class="row" style="display:flex; justify-content:flex-end;">
                    <button class="btn waves-effect waves-light indigo" type="submit" name="action">Save Order
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection



<style>
    .order-form-container {
        margin-top: 6rem !important;
    }

    .form-error {
        color: red;
        margin-left: 1rem;
    }

</style>
