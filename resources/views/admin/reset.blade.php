<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Imported From Florida - Reset Password</title>

    <link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}" media="screen,projection" />



</head>

<body>
    <div id="app">
        <div class="container">
            <div class="row">
                <div class="col s12" style="margin-top: 6rem !important">
                    <div class="card">
                        <div class="card-content indigo white-text">
                            <div class="card-title">Reset Password</div>
                        </div>

                        <div class="card-action">
                            <form method="POST" action="{{ route('admin.password.reset') }}">
                                @csrf
                                @method('PATCH')

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" class="validate" type="password" name="password" required
                                            autocomplete="new-password">

                                        <label for="password">New
                                            Password</label>

                                        @error('password')
                                        <span class="form-error">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="input-field col s12">
                                        <input id="confirm_password" class="validate" type="password"
                                            name="password_confirmation" required autocomplete="new-password">

                                        <label for="confirm_password">Confirm
                                            Password</label>
                                        @error('password_confirmation')
                                        <span class="form-error">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col s12 right-align">
                                        <button type="submit" class="btn btn waves-effect waves-light indigo">
                                            RESET PASSWORD
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    {{-- <script type="text/javascript" src="{{ asset('js/materialize.min.js')}}"></script> --}}
</body>




<style>
    .form-error {
        color: red !important;
    }

</style>


</html>
