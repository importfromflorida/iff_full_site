@component('mail::message')
# Contact Notification

Name: {{ $contact['fname'].' '.$contact['lname'] }}

Topic: {{ $contact['topic'] }}

Email: {{ $contact['email'] }}

Phone: {{ $contact['phone'] }}

Message: {{ $contact['details'] }}



@endcomponent
