@component('mail::message')
# Order Notification

An order has been placed for item:

{{ $order['product']}}

View order for more details.

@component('mail::button', ['url' => 'http://importedfromflorida.co/admin/orders/'.$order['id'] ])
View Order
@endcomponent

{{-- Thanks,<br>
{{ config('app.name') }} --}}
@endcomponent
