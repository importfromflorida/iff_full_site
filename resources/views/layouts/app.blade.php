<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Imported From Florida</title>
    <link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}" type="image/png">




    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}" media="screen,projection" />



</head>

<body>
    <div id="admin-app" class="main">
        <div id="side-nav" class="hide-on-med-and-down">

            <nav>
                <div class="nav-wrapper indigo" style="padding: 0 0 0 1rem;">
                    <ul class="left grey-text text-lighten-1">
                        <li><i class="material-icons" style="font-size: 30px;">dashboard</i></li>
                        <li style="font-size: 30px; margin-left:2rem">Admin</li>
                    </ul>
                </div>
            </nav>

            <ul class="side-nav-item-list" style="margin-top: 3px !important;">
                <li class="menu-item waves-effect"><a href="/">
                        <div>HOME</div>
                    </a></li>
                <li class="menu-item-collapse" style="padding: 0 !important;">
                    <ul class="collapsible collapsible-nav-bar"
                        style="box-shadow: none!important; border: 0 !important; margin: 0 !important;">
                        <li class="blue-grey-text text-lighten-3" style="border-bottom: 1px solid rgba(0, 0, 0, 0.2);">
                            <div class="collapsible-header indigo waves-effect"
                                style="border: 0 !important; display: flex; justify-content: space-between;"
                                onclick="toggleDropDown()">CREATE <i
                                    class="material-icons toggle-left-arrow">keyboard_arrow_left</i><i
                                    class="material-icons toggle-down-arrow" style="display:none;">keyboard_arrow_down
                                </i></div>
                            <div class="collapsible-body" style="border:0!important;">
                                <ul>
                                    <li class="waves-effect {{Route::is('admin.category.create') ? 'active-page' :''}}">
                                        <a href="{{ route('admin.category.create')}}">
                                            <div style="padding: 1rem;">Category</div>
                                        </a></li>
                                    <li class="waves-effect  {{Route::is('admin.product.create') ? 'active-page' :''}}">
                                        <a href="{{ route('admin.product.create')}}">
                                            <div style="padding: 1rem;"> Product</div>
                                        </a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="menu-item waves-effect  {{Route::is('admin.category*') ? 'active-page' :''}}"><a
                        href="{{ route('admin.category')}}">
                        <div>CATEGORIES</div>
                    </a> </li>
                <li class="menu-item waves-effect  {{Route::is('admin.order*') ? 'active-page' :''}}"><a
                        href="{{ route('admin.order')}}">
                        <div> ORDERS</div>
                    </a> </li>
                <li class="menu-item waves-effect  {{Route::is('admin.product*') ? 'active-page' :''}}"><a
                        href="{{ route('admin.product')}}">
                        <div>PRODUCTS</div>
                    </a> </li>
                <li class="menu-item waves-effect"> <a href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                        <div>LOGOUT</div>
                    </a>
                </li>
            </ul>


        </div>



        <main id="main-content">
            <div class="navbar-fixed">
                <nav>
                    <div class="nav-wrapper white" style="padding: 0 0 0 1rem;">
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i
                                class="material-icons black-text">menu</i></a>
                        <ul class="left black-text">
                            <li style="font-size: 1.5em;">@yield('title')</li>
                        </ul>
                    </div>
                </nav>
            </div>

            <ul class="sidenav" id="mobile-demo">
                <li class=""><a href="/">HOME</a></li>
                <li>
                    <ul class="collapsible">
                        <li>
                            <div class="collapsible-header side-nav-toggle-menu" onclick="toggleSideNavDropDown()">
                                CREATE <i class="material-icons side-nav-toggle-menu-left-arrow">keyboard_arrow_left</i>
                                <i class="material-icons  side-nav-toggle-menu-down-arrow"
                                    style="display:none;">keyboard_arrow_down</i>
                            </div>
                            <div class=" collapsible-body ">
                                <ul>
                                    <li class="{{Route::is('admin.category.create') ? 'active-page' :''}}"><a
                                            href="{{route('admin.category.create')}}">Category</a></li>
                                    <li class="{{Route::is('admin.product.create') ? 'active-page' :''}}"><a
                                            href="{{ route('admin.product.create')}}">Product</a></li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </li>
                <li class="{{Route::is('admin.category*') ? 'active' :''}}"><a
                        href="{{route('admin.category')}}">CATEGORIES</a></li>
                <li class=" {{Route::is('admin.order*') ? 'active' :''}}"><a href="{{route('admin.order')}}">ORDERS</a>
                </li>
                <li class="{{Route::is('admin.product*') ? 'active' :''}}">
                    <a href="{{route('admin.product')}}">PRODUCTS</a>
                </li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">LOGOUT </a>
                </li>
            </ul>
            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            @yield('content')
        </main>
    </div>
    {{-- <div id="app" hidden></div> --}}
    <!--JavaScript at end of body for optimized loading-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/materialize.min.js')}}"></script>
    <script type="text/javascript" src={{ asset('js/init.js')}}></script>


    <script>
        function toggleDropDown() {
            var leftArrow = document.querySelector('.toggle-left-arrow')
            var downArrow = document.querySelector('.toggle-down-arrow')

            // Is left arrow showing?
            if (leftArrow.style.display != 'none') {
                leftArrow.style.display = 'none';
                downArrow.style.display = 'block'
            } else {
                downArrow.style.display = 'none'
                leftArrow.style.display = 'block'
            }
        }

        function toggleSideNavDropDown() {
            var leftArrow = document.querySelector('.side-nav-toggle-menu-left-arrow')
            var downArrow = document.querySelector('.side-nav-toggle-menu-down-arrow')

            // Is left arrow showing?
            if (leftArrow.style.display != 'none') {
                leftArrow.style.display = 'none';
                downArrow.style.display = 'block'
            } else {
                downArrow.style.display = 'none'
                leftArrow.style.display = 'block'
            }
        }

    </script>
</body>
<footer class="page-footer white" style="border-top: 1px solid #e0e0e0; color:#444">
    <div class="container">
        <div class="row" style="display: flex; align-items:center; justify-content:center; font-size: 1em;">
            © 2020 Imported From Florida
        </div>
    </div>
</footer>

</html>

<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    .main {
        flex: 1 0 auto;
    }


    #side-nav {
        height: 100%;
        width: 300px;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #3f51b5;
        overflow-x: hidden;
    }

    .side-nav-item-list li {
        font-size: 1em !important;
    }

    .side-nav-item-list li a {
        text-decoration: none !important;
        color: inherit;

    }

    .side-nav-item-list>li>a>div {
        padding: 1rem;
    }

    .menu-item {
        width: 100%;
        color: #b0bec5;
        border-bottom: 1px solid rgba(0, 0, 0, 0.2);
    }

    .menu-item:hover {
        background-color: rgba(0, 0, 0, 0.1);
    }

    .collapsible-nav-bar {
        padding: 0 !important;
    }

    .collapsible-nav-bar .collapsible-body {
        padding: 0rem !important;
    }

    .collapsible-body ul li {
        width: 100%;
        /* font-siz; */
    }

    .collapsible-body ul li:hover {
        background-color: rgba(0, 0, 0, 0.1);
    }

    .side-nav-toggle-menu {
        color: rgba(0, 0, 0, 0.87);
        font-size: 14px;
        font-weight: 500;
        height: 48px;
        line-height: 48px;
        padding: 0 32px !important;
        display: flex;
        justify-content: space-between;
    }

    .active-page {
        color: #FFF !important;
        background-color: rgba(0, 0, 0, 0.1);
        /* border: 0 !important; */
    }

    #main-content {
        margin-left: 300px;
        /* Same as the width of the sidenav */
        /* Increased text to enable scrolling */
    }

    .page-footer {
        margin-left: 300px;
    }

    @media only screen and (max-width: 992px) {
        #main-content {
            margin-left: 0px;
        }

        .page-footer {
            margin-left: 0px;
        }
    }

</style>
