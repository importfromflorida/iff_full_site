<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="I-LNOPJtABEaC3_8W0Kh9i6VXLEtUhkAuxYFic8SzXk" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>Imported From Florida</title> --}}
    {!! SEO::generate(true) !!}

    <link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}" media="screen,projection" />

    <!-- Link font awesome -->
    <script src="https://kit.fontawesome.com/9cfdaf8159.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&display=swap" rel="stylesheet">

</head>

<body>
    <div id="app">
        <main class="main-content">
            {{-- @if ($query ?? '')
            <p>jello</p>
            @endif --}}
            <nav-component :is-auth="{{ auth()->check() ? 1 : 0 }}"> </nav-component>
            @yield('content')
        </main>

        <div class="footer">
            <foot-component> </foot-component>
        </div>

    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/materialize.min.js')}}"></script>
    <script type="text/javascript" src={{ asset('js/spainit.js')}}></script>
</body>

</html>

<style>
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    html,
    body {
        height: 100%;
        margin: 0;
    }

    #app {
        height: 100%;
        display: flex;
        flex-direction: column;
    }

    .main-content {
        flex: 1 0 auto;
    }

    .footer {
        flex-shrink: 0;
    }

</style>
