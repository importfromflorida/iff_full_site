@extends('layouts.main')


@section('content')
<slider-component></slider-component>
<search-component></search-component>
<categories-component :categories="{{ $categories }}"></categories-component>
<about-component></about-component>
@endsection
