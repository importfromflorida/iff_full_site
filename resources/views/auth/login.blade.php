<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Imported From Florida - Login</title>
    <link rel="shortcut icon" href="{{ URL::asset('favicon.png') }}" type="image/png">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}" media="screen,projection" />



</head>

<body>
    <div id="admin-app">
        <div class="container">
            <div class="row">
                <div class="col s12" style="margin-top: 6rem !important">
                    <div class="card">
                        <div class="card-content indigo white-text">
                            <div class="card-title">{{ __('Login') }}</div>
                        </div>

                        <div class="card-action">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="row">
                                    <div class="input-field col s12">

                                        <input id="username" type="text"
                                            class="form-control @error('username') is-invalid @enderror" name="username"
                                            value="{{ old('username') }}" required autocomplete="username" autofocus>

                                        <label for="username" class=" text-md-right">{{ __('Username') }}</label>

                                        @error('username')
                                        <span class="form-error" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">


                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            required autocomplete="current-password">

                                        <label for="password" class=" text-md-right">{{ __('Password') }}</label>
                                        @error('password')
                                        <span class="form-error" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                    </div>
                                </div>

                                <div class="row">
                                    <div class=" offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember"
                                                id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            {{-- <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label> --}}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="" style="display:flex; justify-content: flex-end;">
                                        <button type="submit" class="btn btn waves-effect waves-light indigo">
                                            {{ __('Login') }}
                                        </button>

                                        {{-- @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                        </a>
                                        @endif --}}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    {{-- <script type="text/javascript" src="{{ asset('js/materialize.min.js')}}"></script> --}}
    {{-- <script type="text/javascript" src={{ asset('js/init.js')}}></script> --}}
</body>

</html>
<style>
    .form-error {
        color: red;

    }

</style>
