@extends('layouts.main')


@section('content')
<store-component :products="{{$products}}" :categories="{{$categories}}" search-category="{{ $searchCategory}}">
</store-component>
@endsection
