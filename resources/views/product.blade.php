@extends('layouts.main')


@section('content')
<product-component :product="{{$product}}" :similiar-products="{{$similiarProducts}}"></product-component>
@endsection
