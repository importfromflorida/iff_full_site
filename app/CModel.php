<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CModel extends Model
{
    //
    protected $fillable = ['name'];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
