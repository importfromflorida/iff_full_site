<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'password.reset']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $products = Product::orderBy('name','asc')->with('orders')->get();
        // $products = Product::all()->with('orders');
        return view('admin.product.index',compact('products'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catergories = Category::all()->sortBy('name');
        return view('admin.product.create', compact('catergories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();

        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'categories' => 'required',
            'condition' => 'required',
            'description' => 'sometimes|nullable',
            'color' => 'sometimes|nullable|max:255',
            'mileage' => 'sometimes|nullable|numeric',
            'manufactured' => 'sometimes|nullable',
            'images' => 'required',
            'images.*' => 'mimes:jpeg,png|max:20000'
        ]);



        // Is the number of uploaded images passed the limit(5)?
        if (count($request->file('images')) > 5) return back()->withInput()->withErrors(['images' => 'Maximum images per product is 5']);


        // Is manufactured date given?
        if($request->manufactured !== null) {

            $product->name = $request->name;
            $product->price = $request->price;
            $product->quantity = $request->quantity;
            $product->condition = $request->condition;
            $product->description = $request->description;
            $product->color = $request->color;
            $product->mileage = $request->mileage;
            $product->manufactured = Carbon::parse($request->manufactured);

        }else{
            $product->fill($request->all());
        };

        // Set vehicle property on product
        $product->vehicle = $request->isVehicle ? true : false;

        // Generate slug base on name

        // Replaces any special characters with a hyphen;
        $string = preg_replace('/[^A-Za-z0-9\-]/', '-',strtolower($product->name));
        $product->slug = trim(preg_replace('/-+/', '-', $string), '-');

        $product->save();

        foreach ($request->file('images') as $image) {
            //Store image and get image path
            $imagePath = $image->store('images', 'public');

            // Resize image so each image has a constant size
            $resizedImage = Image::make(public_path("storage/{$imagePath}"))->resize(500,450);
            $resizedImage->save(`storage/{$imagePath}`,75);


            // Get image url
            $imageUrl = Storage::url($imagePath);

            //Save to database
            $product->images()->create([
                'uri' => $imageUrl,
            ]);
        }

        $categories = Category::find($request->categories);

        $product->categories()->attach($categories);

        return redirect()->route('admin.product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $categories = Category::all();

        $nonCategories = array();


        // Gets categories that this product is not under
        foreach ($categories as $item) {
            if(!$product->categories->contains($item->id)){
                $nonCategories[] = $item;
            }
        }
        return view('admin.product.show', compact('product', 'nonCategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'quantity' => 'required||numeric',
            'condition' => 'required|max:255',
            'description' => 'sometimes|nullable',
            'color' => 'sometimes|nullable|max:255',
            'mileage' => 'sometimes|nullable|numeric',
            'manufactured' => 'sometimes|nullable',
        ]);

        // Replaces any special characters with a hyphen;
        $string = preg_replace('/[^A-Za-z0-9\-]/', '-',strtolower($product->name));

        $productSlug =  trim(preg_replace('/-+/', '-', $string), '-');

        $product->name = $request->name;
        $product->slug = $productSlug;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->condition = $request->condition;
        $product->description = $request->description;
        $product->color = $request->color;
        $product->mileage = $request->mileage;
        $product->vehicle = $request->isVehicle ? true : false;
        $product->manufactured = Carbon::parse($request->manufactured);

        // $product->fill($request->all());
        $product->save();
        return redirect()->route('admin.product.show', $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {



        // Delete all images of products in local storage
        foreach ($product->images as $image) {
            $imagePath = str_replace('/storage', '', $image->uri);
            Storage::delete('/public' . $imagePath);
        }

        $product->delete();

        return redirect()->route('admin.product');
    }
}
