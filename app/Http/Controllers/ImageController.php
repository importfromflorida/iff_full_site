<?php

namespace App\Http\Controllers;

use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image as ImageResizer;


class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::findOrFail($request->product_id);

        // Gets number of images this product has
        $imagesStoredCount = $product->images->count();

        $request->validate([
            'images' => 'required',
            'images.*' => 'mimes:jpeg,png|max:20000'
        ]);


        // Is the number of uploaded images + the number of images stored in database passed the limit(5)?
        if (count($request->file('images')) + $imagesStoredCount > 5) return back()->withInput()->withErrors(['images' => 'Maximum images per product is 5']);



        // Loop through images
        foreach ($request->images as $image) {

         //Store image and get image path
         $imagePath = $image->store('images', 'public');

         // Resize image so each image has a constant size
         $resizedImage = ImageResizer::make(public_path("storage/{$imagePath}"))->resize(500,450);
         $resizedImage->save(`storage{$imagePath}`, 75);


        // Get image url
        $imageUrl = Storage::url($imagePath);

        //Save to database
        $product->images()->create([
            'uri' => $imageUrl
        ]);

        }

        return redirect()->route('admin.product.show', $product->id)->with('success','Image(s) uploaded successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {

        $imagePath = str_replace('/storage', '', $image->uri);
        Storage::delete('/public' . $imagePath);
        $image->delete();
        return redirect()->route('admin.product.show', $image->product->id)->with('success','Image(s) deleted successfully!');;
    }

    public function resize(){
        $images = Image::all();

        foreach ($images as $image) {
            $imagePath = str_replace('/storage', '', $image->uri);
            $resizedImage = ImageResizer::make(public_path("storage/{$imagePath}"))->resize(500,450);
            $resizedImage->save(`storage{$imagePath}`, 75);
        }
        return 'Images successfully resized';
    }
}
