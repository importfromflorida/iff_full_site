<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SEO;

class SpaController extends Controller
{

    public function index() {
        SEO::setTitle('Imported From Florida');
        SEO::setDescription('IFF is committed to offering competitive rates and quick service to all our customers. Service tailored to your needs. We intend to work hard to understand your needs in order to deliver low costs to you and a productive, long-term partnership.');
        SEO::opengraph()->setUrl('https://importedfromflorida.co');
        SEO::setCanonical('https://importedfromflorida.co/');
        SEO::jsonLd()->addImage('https://importedfromflorida.co/assets/logo2.png');


        $categories = Category::orderBy('name')->get();

        return view('home', compact('categories'));
    }

    public function store(Request $request, $slug = null) {
        SEO::setTitle('Imported From Florida');
        SEO::setDescription('IFF is committed to offering competitive rates and quick service to all our customers. Service tailored to your needs. We intend to work hard to understand your needs in order to deliver low costs to you and a productive, long-term partnership.');
        SEO::opengraph()->setUrl('https://importedfromflorida.co/store'. ($slug ? '/'.$slug : ''));
        SEO::setCanonical('https://importedfromflorida.co/store'. ($slug ? '/'.$slug : ''));
        SEO::jsonLd()->addImage('https://importedfromflorida.co/assets/logo2.png');


        $categories = Category::all();
        $products = Array();
        $searchCategory = $slug ?? 'all';
        $query=null;
        // Is category slug given?
        if($slug) {
             // Get category from slug
            $category = Category::where('slug', '=', $slug)->first();

            // Is search query given
            if($request->has('sq')){
                $query = $request->get('sq');

                // Get all products related to the given category and search query
                $products = Product::whereHas('categories', function($q) use($category) {
                    $q->where('categories.id',$category->id);
                })->where('name', 'LIKE', '%'.$query.'%')->with('images')->get();

            }else {

                // Get all products related to the given category
                $products = $category->products->load('images');
            }
        }else{
            // Is search query given?
             if($request->has('sq')){
                 $query = $request->get('sq');
                $products = Product::with('images')->where('name', 'LIKE', '%'.$query.'%')->get();

            }else {
                $products = Product::with('images')->get();
            }
        }

      return view('store',compact('products','categories','searchCategory', 'query'));
    }

    public function filterVehicles(Request $request, Product $product) {

     // Get all images for each product
     $vehicles = $product->with('images')->newQuery();

     // Get only vehicles from products
     $vehicles->where('vehicle', '=', 1);
     //vehicles?brand=&model=bmw&type=van&year_from=2000&year_to=2020&price_from=500&price_to=600&hd=lh

      // Search for a vehicles based on their brand.
     if($request->has('brand')) {
         $vehicles->where('brand' ,$request->get('brand'));
     }

     // Search for a vehicles based on their model
     if($request->has('model')) {
         $vehicles->where('model', $request->get('model'));
     }

     //  Search for a vehicles based on their type
    if($request->has('type')) {
         $vehicles->where('type', $request->get('type'));
    }

    //  Search for a vehicles based on their year from
    if($request->has('year_from')) {
        $yFrom = Carbon::create($request->get('year_from'))->toDateTimeString();
         $vehicles->where('manufactured', '>=', $yFrom);
    }

    //  Search for a vehicles based on their year to
    if($request->has('year_to')) {
        $yTo = Carbon::create($request->get('year_to'), 12 ,31)->toDateTimeString();
        $vehicles->where('manufactured', '<=', $yTo);
    }

    //  Search for a vehicles based on their price from
    if($request->has('price_from')) {
        $vehicles->where('price', '>=', $request->get('price_from'));
    }

    //  Search for a vehicles based on their price to
    if($request->has('price_to')) {
        $vehicles->where('price', '<=', $request->get('price_to'));
    }

    //  Search for a vehicles based on their handdrive
    if($request->has('hd')) {
        $vehicles->where('lrhd', $request->get('hd'));
    }


     return view('vehicles', compact('vehicles'));


    }

    public function details($slug) {


        $product = Product::with('images')->where('slug', '=', $slug)->first();

        // Get id of the categories the product are base on
        $categoriesIds  = $product->categories->pluck('id')->toArray();

        // Get 5 other products with same categories
        $similiarProducts = Product::whereHas('categories', function($q) use($categoriesIds){
            $q->whereIn('categories.id', $categoriesIds);
        })->where('id', '!=',$product->id)->with('images')->limit(4)->get();



        SEO::setTitle('Imported From Florida: '.$product->name);
        SEO::setDescription($product->name);
        SEO::opengraph()->setUrl('https://importedfromflorida.co/prdocut/'.$slug);
        SEO::setCanonical('https://importedfromflorida.co/product/'.$slug);
        SEO::jsonLd()->addImage('https://importedfromflorida.co/assets/logo2.png');

        return view('product',compact('product','similiarProducts'));
    }
    public function categories() {

        $categories = Category::orderBy('name')->get();

        return json_encode($categories);
    }
    public function categoryOther() {

        $category = Category::where('name', '=', 'Other')->get();

        return json_encode($category);
    }

    public function getProducts() {
        $products = Product::with('images','categories')->where('quantity', '!=', '0' )->get();

        // return json_encode('hello world!');
        return  json_encode($products);

    }

    public function testing(Request $request) {

        return json_encode($request->all());
    }
}
