<?php

namespace App\Http\Controllers;
use App\Mail\OrderMail;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'password.reset'])->except('store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('created_at','desc')->with('product')->get();
        return view('admin.order.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.order.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::findOrFail($request->product_id);
        $statement = DB::select("SHOW TABLE STATUS LIKE 'orders'");
        $nextId = $statement[0]->Auto_increment;

        $validator = Validator::make($request->all(),[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'questions' => 'nullable',
        ]);

        if($validator->fails()) {
            return response()->json($validator->messages())->setStatusCode(201, 'Validation errors');;
        }


        $orderDetails = [
            'id' => $nextId,
            'product' => $product->name,
        ];

        Mail::mailer('smtp')->to('contact@importedfromflorida.co')->send(new OrderMail($orderDetails));

        $fullName = $request->first_name.' '.$request->last_name;

        $product->orders()->create([
            'name' => $fullName,
            'email' => $request->email,
            'phone' => $request->phone,
            'questions' => $request->questions,
            'status' => 'Pending',
            'quantity' => 0
        ]);





        return json_encode('Supplier Contacted Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('admin.order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        // return $order;
        return view('admin.order.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {

        // return $order->product;

        $request->validate([
            'status' => 'required',
            'quantity' => 'required|numeric'
        ]);

        if($request->status === $order->status) return redirect()->route('admin.order');

        if ($request->status === "Completed") {

            $order->product->quantity = $order->product->quantity - $request->quantity;

            $order->product->save();

        }else {

            $order->product->quantity = $order->product->quantity + $request->quantity;
            $order->product->save();

        }

        $order->fill($request->all());
        $order->save();

        return redirect()->route('admin.order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect()->route('admin.order');
    }
}
