<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
class ContactController extends Controller
{
    public function contact(Request $request) {

         $validator = Validator::make($request->all(),[
            'fname' =>  'required',
            'lname' =>  'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'topic' => 'required',
            'details' => 'required',
        ]);

          if($validator->fails()) {
            return response()->json($validator->messages())->setStatusCode(201, 'Validation errors');;
        }



         Mail::mailer('smtp')->to('contact@importedfromflorida.co')->send(new ContactMail($request->all()));


        return json_encode('Message Sent!');

    }
}
