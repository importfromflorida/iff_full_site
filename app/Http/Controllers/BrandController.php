<?php

namespace App\Http\Controllers;

use App\Brand;
use App\CModel;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $modelList =   array(
        //     array(
        //         'name' => 'AUDI',
        //         'models' => array(),
        //     ),

        //     array(
        //         'name' => 'HINO',
        //         'models' => array(
        //             'RANGER'
        //         )
        //     )
        //     );

        $modelList = array(
            array(
                'name' => 'AUDI',
                'models' => array()
            ),

            array(
                'name' => 'BMW',
                'models' => array()
            ),

            array(
                'name' => 'DAIHATSU',
                'models' => array('ATRAI WAGON', 'BOON', 'COPEN', 'HIJET', 'HIJET TRUCK', 'MIDGETII', 'MIRA', 'ROCKY')
            ),
            array(
                'name' => 'HINO',
                'models' => array('DUTRO', 'RANGER')
            ),
            array(
                'name' => 'HONDA',
                'models' => array('ACCORD', 'BEAT', 'CIVIC', 'CR-V', 'FIT', 'FIT HYBRID', 'FIT SHUTTLE', 'STREAM', 'VEZEL')
            ),
            array(
                'name' => 'ISUZU',
                'models' => array('ELF', 'ELF TRUCK', 'FORWARD')
            ),
            array(
                'name' => 'KAWASAKI',
                'models' => array('KAWASAKI')
            ),
            array(
                'name' => 'LAND ROVER',
                'models' => array()
            ),
            array(
                'name' => 'MAZDA',
                'models' => array('AXELA SPORT', 'BONGO VAN', 'CAROL ECO', 'CX-5', 'DEMIO', 'Eunos Cosmo', 'Eunos Prreso', 'FLAIR', 'VERISA')
            ),
            array(
                'name' => 'MERCEDES BENZ',
                'models' => array('C-Class', 'E CLASS')
            ),
            array(
                'name' => 'MITSUBISHI',
                'models' => array('CANTER', 'CANTER DUMP', 'Delica Starwagon', 'FTO', 'FUSO FIGHTER', 'LANCER EVOLUTION', 'MIRAGE', 'OUTLANDER PHEV', 'PAJERO', 'PAJERO MINI', 'ROSA', 'RVR')
            ),
            array(
                'name' => 'NISSAN',
                'models' => array('AD VAN', 'BLUEBIRD', 'Bluebird Sylphy', 'CARAVAN VAN', 'Cedric Hardtop', 'DUALIS', 'ELGRAND', 'JUKE', 'LARGO', 'LATIO', 'LAUREL', 'MARCH', 'NOTE', 'PRESIDENT JS', 'SERENA', 'SILVIA', 'SKYLINE', 'Skyline Coupe', 'SKYLINE GT-R', 'STAGEA', 'TIIDA', 'VANETTE VAN', 'WINGROAD', 'X-TRAIL', 'XTRAIL')
            ),
            array(
                'name' => 'PEUGEOT',
                'models' => array('206')
            ),
            array(
                'name' => 'RENAULT',
                'models' => array('ALPINE')
            ),
            array(
                'name' => 'ROVER MINI',
                'models' => array('MINI')
            ),
            array(
                'name' => 'SUBARU',
                'models' => array('FORESTER', 'IMPREZA G4', 'IMPREZA WRX', 'LEGACY TOURING WAGON', 'OUTBACK', 'SAMBAR', 'Sambar Truck', 'VIVIO')
            ),
            array(
                'name' => 'SUZUKI',
                'models' => array('ALTO', 'ALTO ECO', 'ALTO WORKS', 'CAPPUCCINO', 'CARRY TRUCK', 'ESCUDO', 'GRAND ESCUDO', 'JIMNY', 'SOLIO', 'SWIFT', 'WAGON R')
            ),
            array(
                'name' => 'TADANO',
                'models' => array()
            ),
            array(
                'name' => 'TOYOTA',
                'models' => array('ALLION', 'ALPHARD', 'AQUA', 'ARISTO', 'AURIS', 'CARINA', 'CELICA', 'CELSIOR', 'CHASER', 'COROLLA AXIO', 'COROLLA FIELDER', 'COROLLA RUNX', 'COROLLA SPACIO', 'CROWN', 'Crown Athlete Series', 'CROWN MAJESTA', 'DYNA', 'FORKLIFT', 'HARRIER', 'HIACE COMMUTER', 'HIACE REGIUS', 'HIACE VAN', 'HIACE WAGON', 'HILUX SURF', 'IPSUM', 'ISIS', 'IST', 'LAND CRUISER', 'LAND CRUISER PRADO', 'LITE ACE NOAH', 'MARK II', 'MARK X', 'MARK X ZIO', 'MR2', 'NOAH', 'PASSO', 'PIXIS EPOCH', 'PLATZ', 'PORTE', 'PREMIO', 'PROBOX VAN', 'PROBOX WAGON', 'RACTIS', 'RAUM', 'RAV4', 'REGIUS ACE', 'REGIUS VAN', 'REGIUSACE VAN', 'RUSH', 'SERA', 'SIENTA', 'STARLET', 'SUCCEED VAN', 'SUCCEED WAGON', 'SUPRA', 'TOURING HIACE', 'TOWNACE NOAH', 'TOYOACE', 'VELLFIRE', 'VITZ', 'VOXY')
            ),
            array(
                'name' => 'VOLKSWAGEN',
                'models' => array('POLO')
            ),);



        foreach ($modelList as $item) {
           $brand = Brand::create([
                'name' => $item['name']
           ]);

           foreach ($item['models'] as $model) {
               $brand->models()->create([
                   'name' => $model
               ]);
           };
        };

        return 'ALL BRANDS WERE CREATED!';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }
}
