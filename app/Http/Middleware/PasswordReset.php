<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PasswordReset
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        

        if(Auth::user()->first_login) {
            return redirect()->route('admin.password.reset');
        }

        return $next($request);
    }
}
