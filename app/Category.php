<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function formatDate($date) {
        return  Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d M Y');
    }
}
