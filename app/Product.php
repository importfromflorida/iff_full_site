<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','price','quantity', 'description', 'condition', 'mileage' , 'manufactured'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class)->orderBy('created_at', 'DESC')  ;
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function formatDate($date) {
        return  Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d M Y');
    }


    public function limitChecker($count){
        return $count < 5;

    }
}
