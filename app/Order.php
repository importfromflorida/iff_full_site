<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    protected $fillable = [
        'name','email', 'phone', 'status' , 'quantity', 'questions'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function formatDate($date) {
        return  Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d M Y');
    }

    public function formatShowDate($date) {
        return  Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('l jS F Y');
    }
}
