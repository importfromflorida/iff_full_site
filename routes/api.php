<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/categories', 'SpaController@categories');
// Route::get('/categories/other', 'SpaController@categoryOther');
Route::get('/products', 'SpaController@getProducts');
Route::post('/contact', 'ContactController@contact');
Route::post('/order/create', 'OrderController@store');

