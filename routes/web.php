<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Mail\Markdown;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ordermail', function () {
     $markdown = new Markdown(view(), config('mail.markdown'));

    return $markdown->render('emails.order');
});

Auth::routes(['register' => false, 'reset' => false]);


Route::get('/', 'SpaController@index')->name('home');
Route::get('/store/{slug?}', 'SpaController@store')->name('storefront');
Route::get('/product/{slug}', 'SpaController@details')->name('product.details');
Route::get('/vehicles', 'SpaController@filterVehicles')->name('product.filter');

// Route::get('/resize', 'ImageController@resize');
Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
Route::get('/admin/password/reset', 'SettingController@reset')->name('admin.password.reset');
Route::patch('/admin/password/reset', 'SettingController@passwordResetUpdate')->name('admin.password.reset');

Route::get('/admin/products', 'ProductController@index')->name('admin.product');
Route::get('/admin/products/create', 'ProductController@create')->name('admin.product.create');
Route::get('/admin/products/{product}', 'ProductController@show')->name('admin.product.show');

Route::post('/admin/products/create', 'ProductController@store')->name('admin.product.store');
Route::get('/admin/products/{product}/edit', 'ProductController@edit')->name('admin.product.edit');
Route::patch('/admin/products/{product}/edit', 'ProductController@update')->name('admin.product.update');
Route::delete('/admin/products/{product}', 'ProductController@destroy')->name('admin.product.destroy');

Route::get('/admin/orders', 'OrderController@index')->name('admin.order');
// Route::get('/orders/create/{id}', 'OrderController@create')->name('admin.order.create');
Route::get('/admin/orders/{order}', 'OrderController@show')->name('admin.order.show');
// Route::post('/admin/orders/create', 'OrderController@store')->name('admin.order.store');
Route::get('/admin/orders/{order}/edit', 'OrderController@edit')->name('admin.order.edit');
Route::patch('/admin/orders/{order}/edit', 'OrderController@update')->name('admin.order.update');
Route::delete('/admin/orders/{order}', 'OrderController@destroy')->name('admin.order.destroy');


Route::get('/admin/categories', 'CategoryController@index')->name('admin.category');
Route::get('/admin/categories/create', 'CategoryController@create')->name('admin.category.create');
Route::get('/admin/categories/{category}', 'CategoryController@show')->name('admin.category.show');
Route::post('/admin/category/create', 'CategoryController@store')->name('admin.category.store');
Route::post('/admin/category/attach', 'CategoryController@attach')->name('admin.category.attach');
Route::get('/admin/categories/{category}/edit', 'CategoryController@edit')->name('admin.category.edit');
Route::patch('/admin/categories/{category}/edit', 'CategoryController@update')->name('admin.category.update');
Route::delete('/admin/categories/{category}', 'CategoryController@destroy')->name('admin.category.destroy');
Route::delete('/admin/categories/detach/{product}/{category}', 'CategoryController@detach')->name('admin.category.detach');


Route::post('/image/create', 'ImageController@store')->name('image.store');
Route::delete('/image/delete/{image}', 'ImageController@destroy')->name('image.destroy');



// Route::post('/admin/contact', 'ContactController@contact')->name('admin.contact');



// Route::get('/{any}', 'SpaController@index')->where('any', '.*');
