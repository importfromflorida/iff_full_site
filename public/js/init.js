(function($) {
    $(function() {
        var d = new Date();
        var currentYear = d.getFullYear()
            // M.AutoInit();
        $('.sidenav').sidenav();
        $('select').formSelect();
        $('.collapsible').collapsible();
        $('.tooltipped').tooltip();
        $('.materialboxed').materialbox();
        $('.modal').modal();
        $('.datepicker').datepicker({
            yearRange: [1900, currentYear],
        });

    }); // end of document ready
})(jQuery)