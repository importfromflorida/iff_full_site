<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $products = Product::all();

        foreach ($products as $product) {
            $string = preg_replace('/[^A-Za-z0-9\-]/', '-',strtolower($product->name));
            $product->slug = trim(preg_replace('/-+/', '-', $string), '-');
            $product->save();
        }
    }
}
