<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = Category::all();

        foreach ($categories as $category) {
            $category->slug =  str_replace(' ', '-',strtolower($category->name));
            $category->save();
        }
    }
}
