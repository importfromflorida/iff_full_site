<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Category;
class ConstantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'admin',
            'username' => 'admin',            
            'password' => Hash::make('fZyR8oM*8*hy'),
            'first_login' => true,
        ]);

        Category::create([
            'name' => 'Furniture'
        ]);
        Category::create([
            'name' => 'Machinary'
        ]);
        Category::create([
            'name' => 'Appliances'
        ]);
        Category::create([
            'name' => 'Vehicles'
        ]);
        Category::create([
            'name' => 'Home Decor'
        ]);
        Category::create([
            'name' => 'Other'
        ]);
    }
}
